package logic

import java.util.*




class Perceptron constructor() {

    lateinit var weights : DoubleArray
    var value: Double = 0.0
    var bias : Double = 1.0
    var delta: Double = 0.0

    constructor(numberOfInputs: Int) : this() {
        val r = Random()

//        bias = Math.random() / 10000000000000.0
//        delta = Math.random() / 10000000000000.0
//        value = Math.random() / 10000000000000.0

        bias =  r.nextDouble()
        weights = DoubleArray(numberOfInputs,
                { _ -> r.nextDouble() })
    }


//    fun derivative(x : Double) : Double {
//        return calculate(x) * (1-calculate(x))
//    }
//

    fun derivative(value: Double): Double {
        return Math.pow(Math.E, value) / Math.pow((1 + Math.pow(Math.E, value)),2.0)
    }

    fun sigmoida(value: Double): Double {
        return 1 / (1 + Math.pow(Math.E, -value))
    }
//
//    fun derivative(value: Double): Double {
//        return value - Math.pow(value, 2.0)
//    }

    fun calculate(input: DoubleArray): Double {
        val inputFixed = doubleArrayOf(1.0)+input
        val sum = inputFixed.indices.sumByDouble { weights[it] * inputFixed[it] }
        return 1.0/(1+Math.exp(-sum))
    }

}

fun main(args: Array<String>) {

}

/**
 * Returns a randomized list.
 */
fun <T> Iterable<T>.shuffle(seed: Long? = null): List<T> {
    val list = this.toMutableList()
    val random = if (seed != null) Random(seed) else Random()
    Collections.shuffle(list, random)
    return list
}