package logic;

import java.awt.*
import java.util.*


class ArmBot constructor(var x: Int, var y: Int, var width: Int, var height: Int) {

    val area: Rectangle = Rectangle(x,y, width,height)

    var alpha : Double = 90.0
    var beta : Double = 180.0

    val sPoint = Point(x, height/2)
    var mPoint = move(sPoint, alpha)
//    var ePoint = move(mPoint, alpha + beta - 90)
    var ePoint = move(mPoint, alpha + beta - 180)

    var clickedPoint = Point(0,0)

    val r = Random()

    init {
        print("Start $sPoint, $mPoint, $ePoint")
    }

    internal fun draw(g: Graphics) {
        val g2 = g as Graphics2D
        g2.stroke = BasicStroke(5f)

        // mark the area
        g2.drawRect(area.x, area.y, area.width, area.height);

        //draw the fist line
        g2.drawLine(sPoint.x,sPoint.y, mPoint.x, mPoint.y)

        //draw the second line
        g2.drawLine(mPoint.x,mPoint.y, ePoint.x, ePoint.y)

        g2.color = Color.GREEN
        g2.drawRect(sPoint.x, sPoint.y, 5, 5);
        g2.color = Color.BLUE
        g2.drawRect(mPoint.x, mPoint.y, 5, 5);
        g2.color = Color.RED
        g2.drawRect(ePoint.x, ePoint.y, 5, 5);


        // last clicked point
        g2.color = Color.PINK
        g2.drawRect(clickedPoint.x, clickedPoint.y, 5, 5);

        // back to true best color
        g2.color = Color.BLACK
    }

    fun checkCoordinates(x: Int, y: Int) {
        if (area.contains(Point(x,y))) {
            println("$x $y")
            clickedPoint = Point(x,y)
        }
    }

    fun setArm(alpha : Double, beta : Double){
        this.alpha = alpha
        this.beta = beta

        mPoint = move(sPoint, alpha)
//        ePoint = move(mPoint, alpha + beta - 90)
        ePoint = move(mPoint, alpha + beta - 180)
    }

    fun setClicked(x: Int, y : Int) {
        clickedPoint = Point(x,y)
    }

    companion object {
        internal var length = 100

        fun move(center: Point, angle: Double): Point {
//            val rad = Math.toRadians(180 - angle)
            val rad = Math.toRadians(angle)
            val x  =   (center.x + length * Math.sin(rad)).toInt() // rzutuj do int
            val y  =   (center.y - length * Math.cos(rad)).toInt() // rzutuj do int
            return Point(x, y)
        }

    }
}