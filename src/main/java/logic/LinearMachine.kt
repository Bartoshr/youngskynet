package logic

import java.util.*

class LinearMachine {
    var white : Perceptron = Perceptron()
    var black : Perceptron = Perceptron()
    val SIZE = 2500

    fun getValue(data: DoubleArray): Int{
        if (black.calculate(data) > white.calculate(data)) {
            return 1
        } else {
            return 0
        }
    }


    fun learnMore(x: Array<DoubleArray>, y: Array<Double>) {
        val r = Random()
        white.weights = DoubleArray(x[0].size,
                { _ -> r.nextDouble() })

        black.weights = DoubleArray(x[0].size,
                { _ -> r.nextDouble() })

        for(n in 0 until 1000) {

            val perm = IntRange(0,x.size-1).toList().shuffle()
            var counter  = 0

            for(p in perm) {
                var isBlack = black.calculate(x[p]) > white.calculate(x[p])

                if (isBlack and (0.0 == y[p])) {
                    for (i in 0 until SIZE) {
                        white.weights[i] += x[p][i]
                        black.weights[i] -= x[p][i]
                    }
                } else if (!isBlack and (1.0 == y[p])) {
                    for (i in 0 until SIZE) {
                        white.weights[i] -= x[p][i]
                        black.weights[i] += x[p][i]
                    }
                } else {
                    counter++
                }
            }

            if (counter == x.size) {
                println("Break happend")
//                println(black.weights.toList())
                break
            }


        }


//        println(black.weights.toList())
//        println(white.weights.toList())
    }

    fun learn(x: DoubleArray, y: Double) {
        val r = Random()
        white.weights = DoubleArray(x.size,
                { _ -> r.nextDouble() })

        black.weights = DoubleArray(x.size,
                { _ -> r.nextDouble() })

        var isBlack = black.calculate(x) > white.calculate(x)
        if (isBlack and (0.0 == y)) {
            for (i in 0 until SIZE) {
                white.weights[i] += x[i]
                black.weights[i] -= x[i]
            }
        } else if (!isBlack and (1.0 == y)) {
            for (i in 0 until SIZE) {
                white.weights[i] -= x[i]
                black.weights[i] += x[i]
            }
        }

//        println(black.weights.toList())
//        println(white.weights.toList())
    }
}