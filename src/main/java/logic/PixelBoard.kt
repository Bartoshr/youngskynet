package logic

import next.Point
import java.util.*
import java.awt.*


class PixelBoard constructor(var x: Int, var y: Int, var width: Int, var height: Int) {

    val area: Rectangle = Rectangle(x,y, width,height)
    var points: MutableList<Point> = mutableListOf()
    var polygon: MutableList<Point> = mutableListOf()


    val r = Random()

    init {
        println("Start PixelBoard")
    }

    internal fun draw(g: Graphics) {
        val g2 = g as Graphics2D
        g2.stroke = BasicStroke(5f)

        // mark the area
        g2.drawRect(area.x, area.y, area.width, area.height);

        for(point in points) {
            g2.color = Color.GREEN
            g2.drawRect(point.x.toInt(), point.y.toInt(), 1, 1);
        }

        g2.color = Color.RED


        if(polygon.isNotEmpty()) {
            val xs = polygon.map { i -> i.x.toInt() }
            val ys = polygon.map { i -> i.y.toInt() }
            g2.drawPolyline(xs.toIntArray(), ys.toIntArray(), xs.size)
        }

        // back to true best color
        g2.color = Color.BLACK
    }

    fun setPoligon(polygon: MutableList<Point>){
        this.polygon = polygon
    }

    fun addPoint(x: Int, y : Int) {
        println("Moved over ${x},${y}")
        if (area.contains(Point(x,y))) {
            points.add(Point(x.toDouble(),y.toDouble()))
        }
    }

}