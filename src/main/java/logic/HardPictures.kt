package logic

class HardPictures {

    companion object {
        val PICTURES = arrayOf(
                Board(20, 0, "1.png").toArray(),
                Board(20, 0, "2.png").toArray(),
                Board(20, 0, "3.png").toArray(),
                Board(20, 0, "4.png").toArray(),
                Board(20, 0, "5.png").toArray(),
                Board(20, 0, "6.png").toArray()
        )

        val RAW_PICTURES = arrayOf(
                Board(20, 0, "1.png").BitmapRedtoArray(),
                Board(20, 0, "2.png").BitmapRedtoArray(),
                Board(20, 0, "3.png").BitmapRedtoArray(),
                Board(20, 0, "4.png").BitmapRedtoArray(),
                Board(20, 0, "5.png").BitmapRedtoArray(),
                Board(20, 0, "6.png").BitmapRedtoArray()
        )
    }

}