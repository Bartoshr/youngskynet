package logic

import java.awt.Color
import java.awt.Graphics
import java.awt.Point
import java.awt.Rectangle
import java.awt.image.BufferedImage
import javax.imageio.ImageIO
import java.io.File
import java.util.*


class Board constructor(var x: Int, var y: Int, var path: String?){

    var blue = Color(0, 0, 255)
    var black = Color(0, 0, 0)
    var fields = Array(50) { arrayOfNulls<Rectangle>(size = 50) } // tablica pól planszy
    var selected = HashSet<Point>()
    var image : BufferedImage?

    init {
        image = if (path == null) null else ImageIO.read(File(path))
        createFields()
    }

    fun createFields() {
        for (i in 0 until size) {
            for (j in 0 until size) {
                val rectX = x + (i + 1) * width
                val rectY = y + (j + 1) * heigth
                fields[i][j] = Rectangle(rectX, rectY, width, heigth)

                if (image != null) {
                    if (((image!!.getRGB(i, j) shr 16) and 0x0FF) <= 125) {
                        selectField(i, j)
                    }

                }
            }
        }
    }


    fun checkCoordinates(x: Int, y: Int): Point? {
        for (i in 0 until size) {
            for (j in 0 until size) {
                val rect = fields[i][j]
                if (rect!!.contains(x, y)) {
                    return Point(i, j)
                }
            }
        }
        return null
    }

    fun selectField(x: Int, y: Int) {
        val point = Point(x, y)
        if (selected.contains(point)) {
            selected.remove(point)
        } else {
            selected.add(point)
        }
    }

    fun toArray() : DoubleArray{
        var result = DoubleArray(size*size) { 0.0 }
        for (i in 0 until size) {
            for (j in 0 until size) {
                val point =  Point(i,j)
                if(selected.contains(point)) {
                    result[j*size+i] = 1.0
                }
            }
        }
        return result;
    }

    fun fromArray255(array: DoubleArray){
        clearSelecion();
        for (i in 0 until size) {
            for (j in 0 until size) {
                if(array[j*size+i] >= 255) {
                    image!!.setRGB(i, j, Color(0,0,0).rgb)
                } else {
                    image!!.setRGB(i, j, Color(255,255,255).rgb)
                }
            }
        }
        createFields()
    }

    fun toArray255() : DoubleArray{
        var result = DoubleArray(size*size) { 0.0 }
        for (i in 0 until size) {
            for (j in 0 until size) {
                val point =  Point(i,j)
                if(selected.contains(point)) {
                    result[j*size+i] = 255.0
                }
            }
        }
        return result;
    }


    fun BitmapRedtoArray() : DoubleArray{
        var result = DoubleArray(size*size) { 0.0 }
        for (i in 0 until size) {
            for (j in 0 until size) {
                result[j*size+i] = ((image!!.getRGB(i, j) shr 16) and 0x0FF).toDouble()
            }
        }
        return result;
    }

    fun loadImage(path : String?){
        clearSelecion();
        image = if (path == null) null else ImageIO.read(File(path))
        createFields()
    }


    fun addNoise(){
        val r = Random()
        for(i in 1 until 100) {
            val point =  Point(r.nextInt(50),r.nextInt(50))
            // only the red counts, but ¯\_(ツ)_/¯
            image!!.setRGB(point.x, point.y, Color(255,255,255).rgb)
            if(!selected.contains(point)) {
                selectField(r.nextInt(50), r.nextInt(50))
            }
        }
    }

    fun clearSelecion() {
        selected.clear()
    }

    private fun markField(g: Graphics, rect: Rectangle, color: Color) {
        g.color = color
        g.fillRect(rect.x + 1, rect.y + 1, width - 1, heigth - 1)
        g.color = black
    }

    internal fun draw(g: Graphics) {
        // głowny szablon
        for (i in 0 until size) {
            for (j in 0 until size) {
                val rect = fields[i][j]
                g.drawRect(rect!!.x, rect.y, rect.height, rect.width)
                if (selected.contains(Point(i, j))) {
                    markField(g, rect!!, blue)
                }
            }
        }

    }

    companion object {
        internal var width = 5
        internal var heigth = 5
        val size = 50
    }


}