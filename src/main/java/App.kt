import gui.Window

fun main(args: Array<String>) {
    java.awt.EventQueue.invokeLater {
        val window = Window()
        window.isVisible = true
        println("Program started")
    }
}