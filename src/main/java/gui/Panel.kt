package gui

import logic.ArmBot
import logic.PixelBoard
import javax.swing.JPanel
import java.awt.Graphics

internal class Panel : JPanel() {

    var pixelBoard: PixelBoard = PixelBoard(100, 0, 500, 500)

    // Game Mechanics in mouseClicked
    internal fun mouseClicked(mouseX: Int, mouseY: Int) {
//        pixelBoard.addPoint(mouseX,mouseY)
//        repaint()
    }

    internal fun mouseMoved(mouseX: Int, mouseY: Int) {
        pixelBoard.addPoint(mouseX,mouseY)
        repaint()
    }


    override fun paintComponent(g: Graphics) {
        g.clearRect(0, 0, this.width, this.height)
        pixelBoard.draw(g)
    }


}