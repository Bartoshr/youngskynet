package gui

import next.Kohonen
import java.awt.BorderLayout
import java.awt.Button
import java.awt.Toolkit
import java.awt.event.ActionListener
import java.awt.event.MouseEvent
import java.awt.event.MouseMotionListener
import javax.swing.BoxLayout
import javax.swing.JPanel

// do odpalenia gui.Window class

class Window : javax.swing.JFrame() {
    private var panel: Panel? = null
    private var learnButton: Button? = null
    lateinit var kohonen: Kohonen


    init {
        initComponents()
        setSize(800, 600)

        val dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width / 2 - this.getSize().width / 2,
                dim.height / 2 - this.getSize().height / 2);
    }

    private fun initComponents() {

        panel = Panel()
        defaultCloseOperation = javax.swing.WindowConstants.EXIT_ON_CLOSE

        panel!!.addMouseMotionListener(object : MouseMotionListener {
            override fun mouseMoved(evt: MouseEvent?) {
            }

            override fun mouseDragged(evt: MouseEvent?) {
                panelMouseMoved(evt)
            }

        })


        learnButton = Button("Learn")
        learnButton!!.addActionListener(ActionListener { evt ->
            kohonen = Kohonen(panel!!.pixelBoard.points)
            kohonen.learn(100000)
            panel!!.pixelBoard.setPoligon(kohonen.weights)

            panel!!.repaint();
        })

        contentPane.add(panel, BorderLayout.CENTER)

        val buttonsPane = JPanel()
        buttonsPane.layout = BoxLayout(buttonsPane, BoxLayout.PAGE_AXIS)
        buttonsPane.add(learnButton)
        contentPane.add(buttonsPane, BorderLayout.SOUTH)
        pack()
    }


    private fun panelMouseMoved(evt: MouseEvent?) {
        if(evt != null) {
            panel!!.mouseMoved(evt.x, evt.y)
        }
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            java.awt.EventQueue.invokeLater {
                val window = Window()
                window.isVisible = true
                println("Program started")
            }
        }
    }
}
