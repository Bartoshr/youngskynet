
package next

import com.google.gson.Gson
import logic.ArmBot
import java.awt.Point
import java.awt.Rectangle
import java.io.File
import java.util.*



class RobotExamples constructor() {

    var rect : Rectangle? = null
    constructor(rect: Rectangle) : this() {
        this.rect = rect
    }

    var examples: MutableList<Pair<DoubleArray, DoubleArray>> = mutableListOf()

    companion object {
        var NUMBER = 40000

        // coordinates of center in real challenge (x, heigth/2)
        var CENTER = Point(100, 500/2)
        var random = Random()


        fun normalizeX(x: Double) : Double {
            return (((x-100.0)/500)*0.8)+0.1
        }

        fun denormalizeX(x: Double): Double{
            return (((x-0.1)/0.8)*500.0)+100
        }

        fun normalizeY(y: Double) : Double {
            return ((y/500.0)*0.8)+0.1
        }

        fun denormalizeY(y: Double): Double{
            return (((y-0.1)/0.8)*500.0)
        }


        fun normalizeAngle(angle: Double) : Double{
            return ((angle/180)*0.8)+0.1
        }

        fun denormalizeAngle(angle: Double) : Double {
            return ((angle-0.1)/0.8)*180.0
        }

    }


    fun generateNormalizedExamples(): MutableList<Pair<DoubleArray, DoubleArray>> {
        for (i in 0 until NUMBER) {
            val alpha = Math.random() * 180
            val beta = Math.random() * 180
            val tmp = ArmBot.move(CENTER, alpha)
//            val p = ArmBot.move(tmp, alpha + beta - 90)
            val p = ArmBot.move(tmp, alpha + beta - 180)

            if(rect != null && rect!!.contains(Point(p.x, p.y))) {
                var pair = Pair(doubleArrayOf(normalizeX(p.x.toDouble()), normalizeY(p.y.toDouble())),
                        doubleArrayOf(normalizeAngle(alpha), normalizeAngle(beta)))
                examples.add(pair)
                println("x: ${p.x}\ty: ${p.y}\talpha : $alpha\tbeta : $beta")
            }
        }

        return examples
    }


    fun generateAllNormalizedExamples(): MutableList<Pair<DoubleArray, DoubleArray>> {
        for (i in 0 until 180) {
            for (j in 0 until 180) {
                val alpha =  i.toDouble()
                val beta = j.toDouble()
                val tmp = ArmBot.move(CENTER, alpha)
//                val p = ArmBot.move(tmp, alpha + beta - 90)
                val p = ArmBot.move(tmp, alpha + beta - 180)

                if(rect != null && rect!!.contains(Point(p.x, p.y))) {
                    var pair = Pair(doubleArrayOf(normalizeX(p.x.toDouble()), normalizeY(p.y.toDouble())),
                            doubleArrayOf(normalizeAngle(alpha), normalizeAngle(beta)))
                    examples.add(pair)
                    println("x: ${p.x}\ty: ${p.y}\talpha : $alpha\tbeta : $beta")
                }
            }
        }
        println("size: ${examples.size}")

        return examples
    }

}

fun exportToJson(){
    val area: Rectangle = Rectangle(100,0, 500,500)
    val examplesGen = RobotExamples(area)
    var list = examplesGen.generateNormalizedExamples()
    var gson = Gson()
    File("examples.json").writeText(gson.toJson(list))

}

fun main(args: Array<String>) {
    // same rect as in challange
    println("Start")
    val area: Rectangle = Rectangle(100,0, 500,500)
    val examplesGen = RobotExamples(area)
    examplesGen.generateAllNormalizedExamples()

//    exportToJson()

    val x = RobotExamples.normalizeX(250.0)
    val dx = RobotExamples.denormalizeX(x)
    println("x = $x dx  = $dx]")

    val y = RobotExamples.normalizeY(250.0)
    val dy = RobotExamples.denormalizeY(y)
    println("y = $y dy  = $dy]")


}