package next

import org.jfree.data.xy.XYSeries
import org.jfree.chart.ChartUtilities
import java.io.File
import org.jfree.chart.plot.PlotOrientation
import org.jfree.chart.ChartFactory
import org.jfree.data.xy.XYSeriesCollection



class Network {
    var layers: MutableList<Layer> = mutableListOf()

    // number of Perceptron Layers
//    val nppl = arrayOf(1,3,1)
    val nppl = arrayOf(2,20,20,2)

    var learningRate =  0.6

    init {
        for(i in 0 until nppl.size) {
            if(i != 0) {
                layers.add(Layer(nppl[i], nppl[i-1]))
            } else {
                layers.add(Layer(nppl[i], 0))
            }
        }
    }

    fun sigmoida(value: Double): Double {
        return 1 / (1 + Math.pow(Math.E, -value))
    }

    fun derivative(value: Double): Double {
        return value - Math.pow(value, 2.0)
    }

//    fun derivative(value: Double) : Double {
//        return Math.pow(Math.E, value) / Math.pow((1 + Math.pow(Math.E, value)),2.0)
//    }


    fun forward(input : DoubleArray): DoubleArray {

        var output = DoubleArray(layers[layers.size-1].neurons.size)


        // all values are set for input layere
        for(i in 0 until layers[0].neurons.size) {
            layers[0].neurons[i].value = input[i]
        }

        for(k in 1 until layers.size) {
            for(i in 0 until layers[k].neurons.size) {
               var sum = 0.0
                for (j in 0 until layers[k-1].neurons.size) {
                    sum += layers[k].neurons[i].weights[j] * layers[k - 1].neurons[j].value
                }
                sum += layers[k].neurons[i].bias

                layers[k].neurons[i].value = sigmoida(sum)
            }
        }

        // Get output
        for (i in  0 until layers[layers.size - 1].neurons.size) {
            output[i] = layers[layers.size - 1].neurons[i].value
        }

        return output
    }

    fun backward(input: DoubleArray, output: DoubleArray) : Double {

        var newOutput = forward(input)

        for(i in 0 until layers[layers.size-1].neurons.size) {
            var error = output[i] - newOutput[i]
            layers[layers.size-1].neurons[i].delta = error * derivative(newOutput[i])
        }


        for(k in layers.size - 2 downTo 0) {

            for(i in 0 until layers[k].neurons.size) {
                var error = 0.0
                for(j in 0 until layers[k+1].neurons.size) {
                    error += layers[k+1].neurons[j].delta * layers[k+1].neurons[j].weights[i]
                }

                layers[k].neurons[i].delta = error * derivative(layers[k].neurons[i].value)
            }

            for(i in 0 until layers[k+1].neurons.size) {
                 for(j in 0 until layers[k].neurons.size) {
                     layers[k+1].neurons[i].weights[j] += learningRate * layers[k+1].neurons[i].delta * layers[k].neurons[j].value
                     layers[k+1].neurons[i].bias += learningRate * layers[k+1].neurons[i].delta

                }
            }

        }

        var error = (0 until output.size).sumByDouble { Math.abs(newOutput[it] - output[it]) }
        error /= output.size
        return error
    }
}


fun main(args: Array<String>) {

    val net = Network()
    val errors =  XYSeries( "Errors" );


    /* Learning */
    for (i in 0..99999) {
        val inputs = doubleArrayOf(Math.round(Math.random()).toDouble(), Math.round(Math.random()).toDouble())
        val output = DoubleArray(1)
        val error: Double


        if((inputs[0] == 1.0) xor (inputs[1] == 1.0))
            output[0] = 1.0
        else
            output[0] = 0.0

        println("xor(${inputs[0]}, ${inputs[1]}) = ${output[0]}")

        error = net.backward(inputs, output)
        errors.add(i.toDouble(), error)
        System.out.println("Error xor step $i is $error")
    }

    println("Learning completed!")

    /* Test */
    /* Test */
    val inputs = doubleArrayOf(1.0, 1.0)
    val output = net.forward(inputs)

    println(inputs[0].toString() + " or " + inputs[1] + " = " + Math.round(output[0]) + " (" + output[0] + ")")

    // chart creation


    val dataset = XYSeriesCollection()
    dataset.addSeries(errors)

    val xylineChart = ChartFactory.createXYLineChart(
            "Statistic of error function",
            "Number of test",
            "Score",
            dataset,
            PlotOrientation.VERTICAL,
            true, true, false)

    val width = 640   /* Width of the image */
    val height = 480  /* Height of the image */
    val XYChart = File("XYLineChart.jpeg")
    ChartUtilities.saveChartAsJPEG(XYChart, xylineChart, width, height)
}