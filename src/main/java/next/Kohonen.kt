package next

import java.util.ArrayList
import java.util.Random

data class Point(val x: Double, val y:Double)

class Kohonen(private val examples: MutableList<Point>) {

    var weights: MutableList<Point> = ArrayList()
    var rand = Random()
    var iter = 100000
    val neuronCount = 100

    init {
        weights.clear()
        for (i in 0 until neuronCount) {
            val point = Point((rand.nextInt(400) + 1).toDouble(),
                    (rand.nextInt(400) + 1).toDouble())
            weights.add(point)
        }
    }


    fun distance(x1: Double, y1: Double, x2 : Double, y2 : Double) : Double {
        return Math.sqrt(Math.pow(x1 - x2, 2.0) + Math.pow(y1 - y2, 2.0))
    }

    fun findNeighbour(point: Point): Int {
        var result = 0
        var min = java.lang.Double.MAX_VALUE
        for (i in weights.indices) {
            val distance = distance(point.x, point.y, weights[i].x, weights[i].y)
            if (distance < min) {
                result = i
                min = distance
            }
        }
        return result
    }

    fun gauss(w: Int, v: Int): Double {
        var p = Math.abs(w - v).toDouble()
        return Math.exp(-1 * Math.pow(p, 2.0) / (2 * Math.pow(LAMBDA_GAUSS, 2.0)))
    }

    fun calculateWeights(point: Point, n: Int, t: Double) {
        val alfa = 1 - (t - 1) / iter
        var x: Double
        var y: Double
        for (i in -(MAX_DISTANCE.toInt() + 1)..MAX_DISTANCE.toInt()) {
            if (((n + i) < weights.size) && ((n + i) >= 0)) {

                x = weights[n + i].x + (alfa * gauss(n + i, n)
                        * (point.x - weights[n + i].x))

                y = weights[n + i].y + (alfa * gauss(n + i, n)
                        * (point.y - weights[n + i].y))

                weights[n + i] = Point(x, y)
            }
        }
    }


    fun learn(times: Int) {
        // get example
        for(i in 0..times) {
            val point = examples[rand.nextInt(examples.size)]
            val neighbour = findNeighbour(point)
            calculateWeights(point, neighbour, i.toDouble())
        }
    }

    companion object {
        private val LAMBDA_GAUSS : Double = 10.0
        private val MAX_DISTANCE: Double = 6.0
    }


}


