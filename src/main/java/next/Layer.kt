package next

import logic.Perceptron

class Layer constructor(thisLayer: Int, lastLayer: Int){

    var neurons : MutableList<Perceptron> = mutableListOf()

    init {
        for (i in 0 until thisLayer) {
            var perceptron  = Perceptron(lastLayer)
            neurons.add(perceptron);
        }
    }


    fun applyNeurons(templates: Array<DoubleArray>) {
        for(i in 0 until templates.size){
            for (j in 0 until templates[i].size-1) {
                neurons[i].weights[j] = templates[i][j]
            }
            neurons[i].bias = templates[i][templates[i].size-1]
            println(neurons[i].weights.toList())
            println(neurons[i].bias)
        }
    }


}